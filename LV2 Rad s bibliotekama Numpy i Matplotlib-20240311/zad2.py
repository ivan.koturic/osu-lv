"""
Datoteka data.csv sadrži mjerenja visine i mase provedena na muškarcima i
ženama. Skripta zadatak_2.py ucitava dane podatke u obliku numpy polja data pri cemu je u 
prvom stupcu polja oznaka spola (1 muško, 0 žensko), drugi stupac polja je visina u cm, a treci
stupac polja je masa u kg.
"""
import numpy as np
import matplotlib.pyplot as plt
import csv

with open('data.csv', 'r') as f:
    reader = csv.reader(f)
    data = list(reader)
    
data_array = np.delete(data, 0, 0)
data_array = np.array(data_array, dtype=float)

#a) Na temelju velicine numpy polja data, na koliko osoba su izvršena mjerenja?
print(len(data_array))

#b) Prikažite odnos visine i mase osobe pomocu naredbe matplotlib.pyplot.scatter
x = np.array(data_array[:,2])
y = np.array(data_array[:,1])

plt.scatter(x, y)
plt.xlabel('Težina osobe')
plt.ylabel('Visina osobe')
plt.show()

#c) Ponovite prethodni zadatak, ali prikažite mjerenja za svaku pedesetu osobu na slici.
x = np.array(data_array[::50,2])
y = np.array(data_array[::50,1])

plt.scatter(x, y)
plt.xlabel('Težina osobe')
plt.ylabel('Visina osobe')
plt.show()

#d) Izracunajte i ispišite u terminal minimalnu, maksimalnu i srednju vrijednost visine u ovom podatkovnom skupu.
height = data_array[:,1]
print(height.min())
print(height.max())
print(height.mean())

#e) Ponovite zadatak pod d), ali samo za muškarce, odnosno žene. Npr. kako biste izdvojili muškarce, stvorite polje koje zadrži bool vrijednosti 
# i njega koristite kao indeks retka.
ind = (data_array[:,0] == 1)
males_height = data_array[ind,1]
print(males_height.min())
print(males_height.max())
print(males_height.mean())