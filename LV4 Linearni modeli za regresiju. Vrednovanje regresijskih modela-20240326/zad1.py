import pandas as pd
from sklearn import datasets
from sklearn . model_selection import train_test_split
import matplotlib.pyplot as plt
from sklearn . preprocessing import StandardScaler
import sklearn.linear_model as lm
from sklearn.metrics import mean_absolute_error, mean_squared_error, r2_score
"""
Zadatak 4.5.1 Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
Potrebno je izgraditi i vrednovati model koji procjenjuje emisiju C02 plinova na temelju ostalih numerickih ulaznih velicina. 
Detalje oko ovog podatkovnog skupa mogu se pronaci u 3. laboratorijskoj vježbi.
"""

# a) Odaberite željene numericke velicine specificiranjem liste s nazivima stupaca. Podijelite podatke na skup za ucenje i skup za testiranje u omjeru 80%-20%.
data = pd.read_csv('data_C02_emission.csv')
input_variables = [ 'Fuel Consumption City (L/100km)',
    'Fuel Consumption Hwy (L/100km)',
    'Fuel Consumption Comb (L/100km)',
    'Fuel Consumption Comb (mpg)',
    'Engine Size (L)',
    'Cylinders'
]
output_variable = ['CO2 Emissions (g/km)']
X = data[input_variables].to_numpy()
y = data[output_variable].to_numpy()
X_train, X_test, y_train, y_test = train_test_split(X ,y ,test_size = 0.2, random_state = 1)

# b) Pomocu matplotlib biblioteke i dijagrama raspršenja prikažite ovisnost emisije C02 plinova o jednoj numerickoj velicini. Pri tome podatke koji pripadaju skupu za 
# ucenje oznacite plavom bojom, a podatke koji pripadaju skupu za testiranje oznacite crvenom bojom.
plt.scatter(X_train[:, 0], y_train, color="blue", label="trening")
plt.scatter(X_test[:, 0], y_test, color="red", label="test")
plt.xlabel('Fuel Consumption City (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()
plt.scatter(X_train[:, 1], y_train, color="blue", label="trening")
plt.scatter(X_test[:, 1], y_test, color="red", label="test")
plt.xlabel('Fuel Consumption Hwy (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()
plt.scatter(X_train[:, 2], y_train, color="blue", label="trening")
plt.scatter(X_test[:, 2], y_test, color="red", label="test")
plt.xlabel('Fuel Consumption Comb (L/100km)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()
plt.scatter(X_train[:, 3], y_train, color="blue", label="trening")
plt.scatter(X_test[:, 3], y_test, color="red", label="test")
plt.xlabel('Fuel Consumption Comb (mpg)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()
plt.scatter(X_train[:, 4], y_train, color="blue", label="trening")
plt.scatter(X_test[:, 4], y_test, color="red", label="test")
plt.xlabel('Engine Size (L)')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()
plt.scatter(X_train[:, 5], y_train, color="blue", label="trening")
plt.scatter(X_test[:, 5], y_test, color="red", label="test")
plt.xlabel('Cylinders')
plt.ylabel('CO2 Emissions (g/km)')
plt.legend()
plt.show()

# c) Izvršite standardizaciju ulaznih velicina skupa za ucenje. Prikažite histogram vrijednosti jedne ulazne velicine prije i nakon skaliranja. Na temelju dobivenih 
# parametara skaliranja transformirajte ulazne velicine skupa podataka za testiranje.
sc = StandardScaler()
X_train_n = sc.fit_transform(X_train)
X_train_n = sc.transform(X_train)
plt.hist(X_train[:,0], color = 'blue', bins=30) # histogram jedne ulazne velicine prije standardizacije
plt.show()
plt.hist(X_train_n[:,0], color = 'red', bins=30) # histogram te iste ulazne velicine nakon standardizacije
plt.show()

# d) Izgradite linearni regresijski modeli. Ispišite u terminal dobivene parametre modela i povežite ih s izrazom 4.6.
linearModel = lm.LinearRegression()
linearModel.fit(X_train_n, y_train)
print(linearModel.coef_)

# e) Izvršite procjenu izlazne velicine na temelju ulaznih velicina skupa za testiranje. Prikažite pomocu dijagrama raspršenja odnos izmedu stvarnih vrijednosti 
# izlazne velicine i procjene dobivene modelom.
X_test_n = sc.transform(X_test)  # Skalirajte ulazne veličine skupa za testiranje koristeći iste parametre skaliranja kao i za skup za učenje
y_pred = linearModel.predict(X_test_n)  # Izračunajte procjenu izlazne veličine na temelju ulaznih veličina skupa za testiranje
plt.scatter(y_test, y_pred, color='blue')
plt.xlabel('Stvarne vrijednosti CO2 emisije (g/km)')
plt.ylabel('Procjene vrijednosti CO2 emisije (g/km)')
plt.title('Stvarne vrijednosti vs. Procjene vrijednosti')
plt.show()

# f) Izvršite vrednovanje modela na nacin da izracunate vrijednosti regresijskih metrika na skupu podataka za testiranje.
mae = mean_absolute_error(y_test, y_pred)
mse = mean_squared_error(y_test, y_pred)
r2 = r2_score(y_test, y_pred)
print("Srednja apsolutna pogreška (MAE):", mae)
print("Srednja kvadratna pogreška (MSE):", mse)
print("Koeficijent determinacije (R^2):", r2)

# g) Što se dogada s vrijednostima evaluacijskih metrika na testnom skupu kada mijenjate broj ulaznih velicina?
