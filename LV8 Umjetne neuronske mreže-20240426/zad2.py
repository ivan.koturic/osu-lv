from tensorflow import keras
import numpy as np
from matplotlib import pyplot as plt
"""
Napišite skriptu koja ce ucitati izgradenu mrežu iz zadatka 1 i MNIST skup
podataka. Pomocu matplotlib biblioteke potrebno je prikazati nekoliko loše klasificiranih slika iz
skupa podataka za testiranje. Pri tome u naslov slike napišite stvarnu oznaku i oznaku predvidenu 
mrežom.
"""
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()

#ucitavanje izgradjene mreze
model = keras.models.load_model('FCN/brojevi.keras')

# skaliranje slike na raspon [0,1]
x_train_s = x_train.astype("float32") / 255
x_test_s = x_test.astype("float32") / 255

x_train_s = x_train_s.reshape((60000, 28 * 28))
x_test_s = x_test_s.reshape((10000, 28 * 28))

y_pred = model.predict(x_test_s) #predvidanje klase slike
y_pred_labels = np.argmax(y_pred, axis=1) #klasa s najvećom vjerojatnošću

misclassified_idx = np.where(y_pred_labels != y_test)[0] #indeksi slika koje su pogresno klasificirane

fig, axes = plt.subplots(nrows=3, ncols=3, figsize=(8,8))
for i, ax in enumerate(axes.flat):
    idx = misclassified_idx[i]
    ax.imshow(x_test[idx], cmap='gray')
    ax.set_title(f"True: {y_test[idx]} Pred: {y_pred_labels[idx]}")
    ax.axis('off')

plt.show()