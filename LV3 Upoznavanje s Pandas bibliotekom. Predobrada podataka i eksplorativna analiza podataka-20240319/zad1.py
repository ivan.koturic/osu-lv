"""
Skripta zadatak_1.py ucitava podatkovni skup iz data_C02_emission.csv.
Dodajte programski kod u skriptu pomocu kojeg možete odgovoriti na sljedeca pitanja: 
a) Koliko mjerenja sadrži DataFrame? Kojeg je tipa svaka velicina? Postoje li izostale ili 
duplicirane vrijednosti? Obrišite ih ako postoje. Kategoricke velicine konvertirajte u tip 
category. 
h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)? 
i) Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.
"""
import pandas as pd
data = pd.read_csv('data_C02_emission.csv')

#a)
print(len(data)) #broj mjerenja
print(data.info()) #tip svake veličine
print(data.duplicated().sum()) #suma svih dupliciranih veličina
print(data.isnull().sum()) #suma svih null vrijednosti
data.dropna() #brisanje null vrijednosti
data.drop_duplicates() #brisanje dupliciranih vrijednosti
#data = data.astype("category")

#b)Koja tri automobila ima najvecu odnosno najmanju gradsku potrošnju? Ispišite u terminal: ime proizvodaca, model vozila i kolika je gradska potrošnja.
print('Najmanja gradska potrošnja')
print(data.sort_values(by="Fuel Consumption City (L/100km)").head(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])
print('Najveća gradska potrošnja')
print(data.sort_values(by="Fuel Consumption City (L/100km)").tail(3)[['Make', 'Model', 'Fuel Consumption City (L/100km)']])

#c) Koliko vozila ima velicinu motora izmedu 2.5 i 3.5 L? Kolika je prosjecna C02 emisija plinova za ova vozila?
motorSize = data[(data['Engine Size (L)'] >= 2.5) & (data['Engine Size (L)'] <= 3.5)]
print('Vozila sa velicinom motora između 2.5 i 3.5 ima',len(motorSize))
print('Prosječna C02 emisija ovih vozila je', motorSize['CO2 Emissions (g/km)'].mean())

#d) Koliko mjerenja se odnosi na vozila proizvodaca Audi? Kolika je prosjecna emisija C02 plinova automobila proizvodaca Audi koji imaju 4 cilindara? 
audi = data[(data['Make'] == 'Audi')]
print(len(audi),'mjerenja se odnosi na vozila proizvođača Audi')
print(f'Prosječna C02 emisija automobila proizvođača Audi koji imaju 4 cilindra',audi[(audi['Cylinders'] == 4)][['CO2 Emissions (g/km)']].mean())

#e) Koliko je vozila s 4,6,8. . . cilindara? Kolika je prosjecna emisija C02 plinova s obzirom na broj cilindara?
cylinder_counts = data["Cylinders"].value_counts()
print(f"Broj vozila s 4,6,8 .. cilindara ",cylinder_counts)

avgCO2 = data.groupby("Cylinders")["CO2 Emissions (g/km)"].mean()
print(avgCO2)   #prosjecna emisija 

#f) Kolika je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel, a kolika za vozila koja koriste regularni benzin? Koliko iznose medijalne vrijednosti?
dizel = data[(data['Fuel Type'] == 'D')]
print(dizel['Fuel Consumption City (L/100km)'].mean(),'je prosjecna gradska potrošnja u slucaju vozila koja koriste dizel')
print(dizel.median())
benzin = data[(data['Fuel Type'] == 'Z') | (data['Fuel Type'] == 'X')]
print(benzin['Fuel Consumption City (L/100km)'].mean(),'je prosjecna gradska potrošnja u slucaju vozila koja koriste benzin')
print(benzin.median())

#g) Koje vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva?
fourC = data[(data["Cylinders"]==4) & (data["Fuel Type"]=='D')]
print(f"Vozilo s 4 cilindra koje koristi dizelski motor ima najvecu gradsku potrošnju goriva",fourC.sort_values(ascending=False, by='Fuel Consumption City (L/100km)')[['Make', 'Model']].head(1))

#h) Koliko ima vozila ima rucni tip mjenjaca (bez obzira na broj brzina)?
manual = data[data["Transmission"].str.contains("M")]
print("Broj vozila s manualnim mjenjacem ",len(manual))

#i)  Izracunajte korelaciju izmedu numerickih velicina. Komentirajte dobiveni rezultat.
print (f"Korelacija izmedu numerickih velicina \n", data.corr(numeric_only = True))
