"""
Skripta zadatak_3.py ucitava sliku 'road.jpg2' Manipulacijom odgovarajuce
numpy matrice pokušajte:
a) posvijetliti sliku,
b) prikazati samo drugu cetvrtinu slike po širini, 
c) zarotirati sliku za 90 stupnjeva u smjeru kazaljke na satu,
d) zrcaliti sliku
"""

import numpy as np
import matplotlib.pyplot as plt

img = plt.imread("road.jpg")

#a) 
plt.imshow(img, cmap='gray', alpha=0.75)
plt.title('Posvijetljena slika')
plt.show()

#b) 
img_quarter = img[:, img.shape[1] // 4 : img.shape[1] // 2]
plt.imshow(img_quarter, cmap='gray')
plt.title('Druga četvrtina slike po širini')
plt.show()

#c) 
img_rotated = np.rot90(img, k=3)
plt.imshow(img_rotated, cmap='gray')
plt.title('Rotirana slika')
plt.show()

#d) 
img_flipped = np.fliplr(img)
plt.imshow(img_flipped, cmap='gray')
plt.title('Zrcaljena slika')
plt.show()
