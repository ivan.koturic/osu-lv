"""
Napišite programski kod koji ce prikazati sljedece vizualizacije: 
"""

import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('data_C02_emission.csv')

#a) Pomocu histograma prikažite emisiju C02 plinova. Komentirajte dobiveni prikaz.
plt.hist(df['CO2 Emissions (g/km)'])
plt.title('Emisija CO2 plinova')
plt.xlabel('Emisija CO2 (g/km)')
plt.ylabel('Broj vozila')
plt.show()

#b) Pomocu dijagrama raspršenja prikažite odnos izmedu gradske potrošnje goriva i emisije C02 plinova. Komentirajte dobiveni prikaz. Kako biste bolje razumjeli odnose izmedu velicina, obojite tockice na dijagramu raspršenja s obzirom na tip goriva.
df['Fuel Type']=pd.Categorical(df['Fuel Type'])
df.plot.scatter(x='Fuel Consumption City (L/100km)', y='CO2 Emissions (g/km)', c='Fuel Type', cmap = 'seismic', s=20)
plt.show()

#c) Pomocu kutijastog dijagrama prikažite razdiobu izvangradske potrošnje s obzirom na tipgoriva. Primjecujete li grubu mjernu pogrešku u podacima?
df.boxplot( column =['Fuel Consumption Hwy (L/100km)'], by='Fuel Type')
plt.show()

#d) Pomocu stupcastog dijagrama prikažite broj vozila po tipu goriva. Koristite metodu groupby.
fuel_group = df.groupby('Fuel Type')
count_by_fuel = fuel_group['Fuel Type'].count()
count_by_fuel.plot(kind='bar', rot=0)
plt.xlabel('Tip goriva')
plt.ylabel('Broj vozila')
plt.title('Broj vozila po tipu goriva')
plt.show()

#e) Pomocu stupcastog grafa prikažite na istoj slici prosjecnu C02 emisiju vozila s obzirom na broj cilindara.
cylinders_group = df.groupby('Cylinders')
mean_c02_by_cylinders = cylinders_group['CO2 Emissions (g/km)'].mean()
mean_c02_by_cylinders.plot(kind='bar', rot=0)
plt.xlabel('Broj cilindara')
plt.ylabel('Prosječna C02 emisija')
plt.title('Prosječna C02 emisija po broju cilindara')
plt.show()
