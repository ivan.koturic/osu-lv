#Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva SMSSpamCollection.txt
#[1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke oznacene kao  spam, a neke kao ham.
#Primjer dijela datoteke:
#       ham Yup next stop.
#       ham Ok lar... Joking wif u oni...
#       spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff!
#a) Izracunajte koliki je prosjecan broj rijeci u SMS porukama koje su tipa ham, a koliko je 
#prosjecan broj rijeci u porukama koje su tipa spam.
#b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?


#a)Prosječan broj riječi u SMS porukama koje su tipa ham i spam.
fhand = open('SMSSpamCollection.txt')
ham_count = 0
ham_total_words = 0
spam_count = 0
spam_total_words = 0
for line in fhand:
    category, message = line.rstrip().split('\t')
    word_count = len(message.split())
    if category == 'ham':
        ham_count += 1
        ham_total_words += word_count
    else:
        spam_count += 1
        spam_total_words += word_count
ham_avg_words = ham_total_words / ham_count
spam_avg_words = spam_total_words / spam_count
print(f"Prosječan broj riječi u ham porukama: {ham_avg_words:.4f}")
print(f"Prosječan broj riječi u spam porukama: {spam_avg_words:.4f}")

#b)Broj SMS poruka koje su tipa spam i završavaju uskličnikom
fhand = open('C:/Users/student/Desktop/osu_lv/LV1/SMSSpamCollection.txt')
num_exclamations = 0
for line in fhand:
    category, message = line.rstrip().split('\t')
    if category == 'spam' and message.endswith('!'):
        num_exclamations += 1
print('Broj SMS poruka koje su tipa spam a završavaju uskličnikom:', num_exclamations)
