#Zadatak 1.4.4 Napišite Python skriptu koja ce ucitati tekstualnu datoteku naziva song.txt.
#Potrebno je napraviti rjecnik koji kao kljuceve koristi sve razlicite rijeci koje se pojavljuju u 
#datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (kljuc) pojavljuje u datoteci.
#koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih.

word_dict = {}
fhand = open('C:\\Users\\student\\Desktop\\osulv\\osu-lv\\LV1 Uvod u programski jezik Python-20240305\\song.txt', 'r')
for line in fhand:
    words = line.strip().split()
    for word in words:
        if word in word_dict:
            word_dict[word] += 1
        else:
            word_dict[word] = 1
unique_words = [word for word in word_dict if word_dict[word] == 1]
num= len(unique_words)
print(num, "riječi koje se pojavljuju samo jednom:")
for word in unique_words:
    print(word)
# The dictionary 'data' now contains the contents of the text file 