import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from sklearn.datasets import make_classification
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, precision_score, recall_score
from sklearn.metrics import confusion_matrix, ConfusionMatrixDisplay
"""
Skripta zadatak_1.py generira umjetni binarni klasifikacijski problem s dvije
ulazne velicine. Podaci su podijeljeni na skup za ucenje i skup za testiranje modela.
"""
X, y = make_classification(n_samples=200, n_features=2, n_redundant=0, n_informative=2,
                            random_state=213, n_clusters_per_class=1, class_sep=1)

# train test split
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=5)

# a) Prikažite podatke za ucenje u x1 - x2 ravnini matplotlib biblioteke pri cemu podatke obojite
# s obzirom na klasu. Prikažite i podatke iz skupa za testiranje, ali za njih koristite drugi
# marker (npr. 'x'). Koristite funkciju scatter koja osim podataka prima i parametre c i
# cmap kojima je moguce definirati boju svake klase.
plt.scatter(X_train[:,0], X_train[:,1], label='Training', c=y_train, cmap='coolwarm')
plt.scatter(X_test[:,0], X_test[:,1], label='Test', c=y_test, cmap='coolwarm', marker='x')
plt.xlabel('x1')
plt.ylabel('x2')
plt.show()

# b) Izgradite model logisticke regresije pomocu scikit-learn biblioteke na temelju skupa podataka za ucenje. 
# inicijalizacija i ucenje modela logisticke regresije
LogRegression_model = LogisticRegression()
LogRegression_model.fit(X_train, y_train)

# c) Pronadite u atributima izgradenog modela parametre modela. Prikažite granicu odluke naucenog modela u ravnini x1 - x2 zajedno s podacima za ucenje. 
# Napomena: granica odluke u ravnini x1 - x2 definirana je kao krivulja: θ0 + θ1x1 + θ2x2 = 0.
coef = LogRegression_model.coef_[0]
intercept = LogRegression_model.intercept_

def decision_boundary(x1):
    return (-coef[0]*x1 - intercept) / coef[1]

plt.scatter(X_train[:, 0], X_train[:, 1], c=y_train, cmap='coolwarm')
plt.plot(X_train[:, 0], decision_boundary(X_train[:, 0]))
plt.xlabel('x1')
plt.ylabel('x2')
plt.title('Logistic Regression Decision Boundary')
plt.show()


# d) Provedite klasifikaciju skupa podataka za testiranje pomocu izgradenog modela logisticke regresije. Izracunajte i prikažite matricu zabune na testnim podacima. 
# Izracunate tocnost, preciznost i odziv na skupu podataka za testiranje.
# stvarna vrijednost izlazne velicine i predikcija
y_pred = LogRegression_model.predict(X_test)
# tocnost
print("Tocnost : ", accuracy_score(y_test, y_pred))
print("Preciznost : ", precision_score(y_test, y_pred))
print("Odziv: ", recall_score(y_test, y_pred))
# matrica zabune
cm = confusion_matrix(y_test, y_pred)
print("Matrica zabune : ", cm)
disp = ConfusionMatrixDisplay(confusion_matrix(y_test, y_pred))
disp.plot()
plt.show()

# e) Prikažite skup za testiranje u ravnini x1 - x2. Zelenom bojom oznacite dobro klasificirane primjere dok pogrešno klasificirane primjere oznacite crnom bojom. 
plt.scatter(X_test[:, 0], X_test[:, 1], label="test", c=y_test)
for i in range(len(y_test)):
    if y_test[i] == y_pred[i]:
        plt.scatter(X_test[i, 0], X_test[i, 1], c='g')
    else:
        plt.scatter(X_test[i, 0], X_test[i, 1], c='k')

plt.xlabel("x1")
plt.ylabel("x2")
plt.show()

